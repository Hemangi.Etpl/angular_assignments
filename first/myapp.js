var myApp = angular.module("myApp", ['ui.router']);
var base_url = "http://localhost/myangularjs/";
myApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when("", "/menu");
    $urlRouterProvider.otherwise("/menu");
    $stateProvider.state("/home", {
        url: "/home",
        views: {
            'home': {
                templateUrl: "home.html"
            }
        }
    }).state("/about_us", {
        url: "/about_us",
        views: {
            'home': {
                templateUrl: "about_us.html"
            }
        }
    }).state("/contact_us", {
        url: "/contact_us",
        views: {
            'home': {
                templateUrl: "contact_us.html"
            }
        }
    }).state("/menu", {
        url: "/menu",
        views: {
            'home': {
                templateUrl: "menu.html"
            }
        }
    }).state("/products", {
        url: "/products",
        views: {
            'home': {
                templateUrl: "products.html",
                controller: function ($scope) {
                    $scope.products_list = [{name: 'Product 1'}, {name: 'Product 2'}, {name: 'Product 3'}, {name: 'Product 4'},{name: 'Product 5'}, {name: 'Product 6'}, {name: 'Product 7'}, {name: 'Product 8'}];
                }
            }
        }
    });
});